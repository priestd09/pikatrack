FROM ruby:2.5.1

RUN curl -sL https://deb.nodesource.com/setup_9.x | bash - && apt-get install -y nodejs postgis gpsbabel && cd /root/ && git clone https://gitlab.com/pikatrack/pikatrack && cd /root/pikatrack/backend && bundle install && cd /root/pikatrack/frontend && npm install