import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import store from './store/index'
import axios from 'axios'
import router from './router/index'
import Notifications from 'vue-notification'

import './assets/sass/application.sass'
import 'leaflet/dist/leaflet.css'
import '@fortawesome/fontawesome-free/css/all.min.css'

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(Notifications)

new Vue({
    store: store,
    router,
    render: h => h(App),
    created: function () {
        axios.interceptors.request.use((config) => {
            config.headers.client = window.localStorage.getItem('client')
            config.headers['access-token'] = window.localStorage.getItem('access-token')
            config.headers.uid = window.localStorage.getItem('uid')
            config.headers['token-type'] = window.localStorage.getItem('token-type')
            return config
        })

        axios.interceptors.response.use((response) => {
            // Set user headers only if they are not blank.
            // If DTA gets a lot of request quickly, it won't return headers for some requests
            // so you need a way to keep headers in localStorage to getting set to undefined
            if (response.headers['access-token']) {
                localStorage.setItem('access-token', response.headers['access-token'])
                localStorage.setItem('client', response.headers.client)
                localStorage.setItem('uid', response.headers.uid)
                localStorage.setItem('token-type', response.headers['token-type'])
            }
            // You have to return the response here or you won't have access to it
            // later
            return response
        }, (error) => {

            // Looking for 401 may be problematic in the future when checking for higher levels of authentication like admin but it should be fine for now.
            if (error.response.status === 401) {
                console.log("test")
                localStorage.setItem('access-token', '')
                localStorage.setItem('client', '')
                localStorage.setItem('uid', '')
                localStorage.setItem('token-type', '')
            }
            return Promise.reject(error)
        })
    }
}).$mount('#app')
