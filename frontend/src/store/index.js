import Vue from 'vue'
import Vuex from 'vuex'
import profile from './api/profile'
import user from './api/user'
import activity from './api/activity'
import section from './api/section'
import sectionEfforts from './api/sectionEfforts'
import feed from './api/feed'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        profile,
        user,
        activity,
        section,
        sectionEfforts,
        feed
    }
})
