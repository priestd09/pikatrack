import Vuex from 'vuex'
import Vue from 'vue'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const user = new Vapi({
    baseURL: process.env.VUE_APP_API_BASE,
    state: {
        user: []
    }
}).get({
    action: 'show_user',
    property: 'user',
    path: ({userId}) => `user/${userId}.json`
}).getStore({
    createStateFn: true
})

export default {
    state: user.state,
    mutations: user.mutations,
    actions: user.actions,
    getters: user.getters
}
