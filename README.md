# PikaTrack
<a href="https://liberapay.com/PikaTrack/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

Work in progress project for an activity tracking website primarily for running and cycling. You can follow my progress at https://toot.cafe/@Qwertii

![screenshot](https://itscode.red/images/pikatrack-activity-with-sections.png)
![screenshot](https://i.imgur.com/VI9zZg7.png)
# Features
* Upload GPX files and see them on a map with statistics like total time and distance.
* Crop bits of your activity to create "Sections". Every time you follow these sections again your activity will be matched against them and your time for that section displayed.
* Feed showing your previous activities.

# Future features
* Android app
* Compare times on sections against friend times.
* Follow friends and see their activites in a feed
* More analysis of data


# Development

This app uses VueJS for the frontend and Rails API mode for the backend. 

To get everything set up check out the [contributing guide](https://gitlab.com/pikatrack/pikatrack/blob/master/CONTRIBUTING.md).

# Install on production

Take a look at the [docker deploy repo](https://gitlab.com/pikatrack/docker-deploy)