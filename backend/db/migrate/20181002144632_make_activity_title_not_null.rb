class MakeActivityTitleNotNull < ActiveRecord::Migration[5.2]
    def change
        change_column :activities, :title, :string, null: false, default: 'Untitled Activity'
    end
end
