require 'rails_helper'

RSpec.describe "Activity upload fixer" do
    it 'converts fit files to gpx' do
        converted = ActivityUploadFixer.fix_file(File.open("#{Rails.root}/spec/files/example_fit_file.fit"), 'application/vnd.ant.fit')
        expect(IO.readlines(converted.path)[0]).to eq("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
    end
end