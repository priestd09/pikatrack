require 'rails_helper'

RSpec.describe "Feed API" do
    before(:each) do
        @current_user = FactoryBot.create(:user)
    end

    # it 'creates activity' do
    #     login
    #
    #     params = {
    #         title: "A test activity",
    #         description: "A test activity...",
    #         original_activity_log_file: fixture_file_upload('files/test_gpx.xml', 'text/xml')
    #     }
    #
    #
    #     post 'activity', params
    # end


    def login
        post '/auth/sign_in', params:  { email: @current_user.email, password: 'password' }.to_json, headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    end
    def get_auth_params_from_login_response_headers(response)
        {
            'access-token' => response.headers['access-token'],
            'client' => response.headers['client'],
            'uid' => response.headers['uid'],
            'expiry' => response.headers['expiry'],
            'token_type' => response.headers['token-type']
        }
    end
end