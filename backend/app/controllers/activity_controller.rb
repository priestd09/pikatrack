class ActivityController < ApplicationController
    before_action :authenticate_user!, only: [:create, :destroy]

    def index
        @activities = Activity.visible(current_user).where(user_id: params[:id]).where.not(computed_activities: {id: nil}).order(created_at: :desc)
    end

    def show
        @activity = Activity.visible(current_user).find_by(id: params[:id])
        if @activity.nil?
            render :nothing => true, :status => 404
        else
            render json: {error: 'activity still processing'} unless @activity.computed_activity
        end
    end

    def create
        @activity = Activity.new(activity_params.merge({user: current_user}))
        if @activity.valid?
            @activity.save
            ActivityWorker.perform_async(@activity.id)
            render 'activity/create', formats: [:json]
        else
           render json: {error: @activity.errors}, status: :bad_request
        end
    end

    def destroy
        activity = Activity.find(params[:id])
        activity.destroy if activity.user_id == current_user.id
    end

    def activity_params
        params.require(:activity).permit(:title, :description, :privacy, :activity_type, :original_activity_log_file)
    end
end
