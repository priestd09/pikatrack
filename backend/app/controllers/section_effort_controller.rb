class SectionEffortController < ApplicationController
    def index
        if params[:section_id] && params[:user_id]
            @section_efforts = SectionEffort.includes(:activity).where(section_id: params[:section_id], activities: {user_id: params[:user_id], privacy: :public_activity}).order(:time)
        elsif params[:section_id] && current_user
            @section_efforts = SectionEffort.includes(:activity).where(section_id: params[:section_id], activities: {user_id: current_user.id}).order(:time)
        end
        render 'section_effort/index', formats: [:json]
    end
end
