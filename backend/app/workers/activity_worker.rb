class ActivityWorker
    require 'open-uri'
    require 'path_compare'
    include Sidekiq::Worker

    def perform(activity_id)
        activity = Activity.find(activity_id)
        fixed_file = ActivityUploadFixer.fix_file(
            open(Rails.application.routes.url_helpers.rails_blob_url(activity.original_activity_log_file)),
            activity.original_activity_log_file.blob.content_type,
            activity.user.id
        )

        gpx = GPX::GPXFile.new(gpx_file: fixed_file.path)
        ca = ComputedActivity.create(
            distance: gpx.distance,
            start_time: gpx.tracks.first.segments.first.points.first.time,
            end_time: gpx.tracks[-1].segments[-1].points[-1].time,
            moving_time: gpx.moving_duration,
            elevation: 0,
            activity: activity
        )


        ca.activity_log_file.attach(
            io: fixed_file,
            filename: "ActivityLog.gpx",
            content_type: 'application/gpx+xml') # TODO: Change this when privacy zones are added
        ca.save_matching_sections
    end
end
