# Takes user uploaded files and then standardises them in to the simplest possible format while retaining all important data.

# These tempfiles seem unsafe. Check this again later
module ActivityUploadFixer
    def ActivityUploadFixer.fix_file(activity_file, file_type, current_user)
        if file_type == 'application/vnd.ant.fit'
            final_file = fit_to_gpx(activity_file)
        elsif file_type == 'application/gpx+xml'
            final_file = activity_file
        else
            raise "Unsupported file type #{file_type}"
        end

        return final_file
    end

    def ActivityUploadFixer.fit_to_gpx(activity_file)
        tempfile = Tempfile.new()
        GPSBabel.convert({
             input: {
                 format: 'garmin_fit',
                 file: activity_file.path
             },
             output: {
                 format: 'gpx',
                 file: tempfile.path
             }
         })
        return tempfile
    end

    # GPX supports multiple track segments. Some logging apps create a new segment after they have been paused and resumed.
    # We don't care about this extra data so lets group it in to one track segment.
    def ActivityUploadFixer.join_trkseg

    end

    def crop_privacy_zone(activity_file, current_user)

    end
end